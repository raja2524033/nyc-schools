package com.example.myapplication

import com.example.myapplication.network.RetrofitApi
import com.example.myapplication.network.SchoolsApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SchoolsRepo constructor(private val schoolsApi: RetrofitApi)
{

    suspend fun getSchoolsList(): Result<List<SchoolsListItem>> = withContext(Dispatchers.IO){
         kotlin.runCatching {
            schoolsApi.buildRetrofit().geSchoolsData()
        }

    }
}