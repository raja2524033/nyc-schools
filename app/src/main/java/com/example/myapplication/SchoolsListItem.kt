package com.example.myapplication

import com.google.gson.annotations.SerializedName

data class SchoolsListItem(

    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("school_sports")
    val schoolSports: String,
    @SerializedName("seats101")
    val seats101: String,
    @SerializedName("seats1010")
    val seats1010: String,
    @SerializedName("seats102")
    val seats102: String,
    @SerializedName("seats103")
    val seats103: String,
    @SerializedName("seats104")
    val seats104: String,
    @SerializedName("seats105")
    val seats105: String,
    @SerializedName("seats106")
    val seats106: String,
    @SerializedName("seats107")
    val seats107: String,
    @SerializedName("seats108")
    val seats108: String,
    @SerializedName("seats109")
    val seats109: String,
    @SerializedName("seats1specialized")
    val seats1specialized: String,
    @SerializedName("seats2specialized")
    val seats2specialized: String,
    @SerializedName("seats3specialized")
    val seats3specialized: String,
    @SerializedName("seats4specialized")
    val seats4specialized: String,
    @SerializedName("seats5specialized")
    val seats5specialized: String,
    @SerializedName("seats6specialized")
    val seats6specialized: String,
    @SerializedName("seats9ge1")
    val seats9ge1: String,
    @SerializedName("seats9ge10")
    val seats9ge10: String,
    @SerializedName("seats9ge2")
    val seats9ge2: String,
    @SerializedName("seats9ge3")
    val seats9ge3: String,
    @SerializedName("seats9ge4")
    val seats9ge4: String,
    @SerializedName("seats9ge5")
    val seats9ge5: String,
    @SerializedName("seats9ge6")
    val seats9ge6: String,
    @SerializedName("seats9ge7")
    val seats9ge7: String,
    @SerializedName("seats9ge8")
    val seats9ge8: String,
    @SerializedName("seats9ge9")
    val seats9ge9: String,
    @SerializedName("seats9swd1")
    val seats9swd1: String,
    @SerializedName("seats9swd10")
    val seats9swd10: String,
    @SerializedName("seats9swd2")
    val seats9swd2: String,
    @SerializedName("seats9swd3")
    val seats9swd3: String,
    @SerializedName("seats9swd4")
    val seats9swd4: String,
    @SerializedName("seats9swd5")
    val seats9swd5: String,
    @SerializedName("seats9swd6")
    val seats9swd6: String,
    @SerializedName("seats9swd7")
    val seats9swd7: String,
    @SerializedName("seats9swd8")
    val seats9swd8: String,
    @SerializedName("seats9swd9")
    val seats9swd9: String,
    @SerializedName("shared_space")
    val sharedSpace: String,
    @SerializedName("specialized")
    val specialized: String,
    @SerializedName("start_time")
    val startTime: String,
    @SerializedName("state_code")
    val stateCode: String,
    @SerializedName("subway")
    val subway: String,
    @SerializedName("total_students")
    val totalStudents: String,
    @SerializedName("transfer")
    val transfer: String,
    @SerializedName("website")
    val website: String,
    @SerializedName("zip")
    val zip: String
)