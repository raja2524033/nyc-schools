package com.example.myapplication

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.network.RetrofitApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch


sealed class ListViewState {
    data class Success(val list: List<SchoolsListItem>) : ListViewState()
    object Loading : ListViewState()
    data class Error(val error: Throwable) : ListViewState()
    object EmptyState : ListViewState()
}

class SchoolsViewModel : ViewModel() {


    val api = RetrofitApi()
    private val schoolsRepo = SchoolsRepo(api)


    private val _uiState = MutableStateFlow<ListViewState>(ListViewState.Loading)


    val uiState = _uiState.asStateFlow()



    init {

        getSchoolsList()
    }
    fun getSchoolsList() {
        viewModelScope.launch {
            schoolsRepo.getSchoolsList().onSuccess {
                if (it.isEmpty()) {
                    _uiState.emit(
                        ListViewState.EmptyState
                    )
                } else {
                    _uiState.emit(ListViewState.Success(it))
                }
            }.onFailure {
                _uiState.emit(ListViewState.Error(it))
            }
        }
    }
}