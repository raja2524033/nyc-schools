package com.example.myapplication.network

import com.example.myapplication.SchoolsListItem
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolsApi {
    @GET("resource/s3k6-pzi2.json")
    suspend fun geSchoolsData(): List<SchoolsListItem>
}

class RetrofitApi {

    fun buildRetrofit(): SchoolsApi {
        return Retrofit.Builder().baseUrl(
            "https://data.cityofnewyork.us"
        ).addConverterFactory(GsonConverterFactory.create()).build()
            .create(SchoolsApi::class.java)
    }
}