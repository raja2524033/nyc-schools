package com.example.myapplication

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.myapplication.ui.theme.MyApplicationTheme

class MainActivity : ComponentActivity() {

    private val viewmodel: SchoolsViewModel = SchoolsViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            MyApplicationTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    showSchoolsList(viewmodel)
                }
            }
        }
    }

    @Composable
    private fun showSchoolsList(viewmodel: SchoolsViewModel) {
        val listState = viewmodel.uiState.collectAsState()


        when (listState.value) {
            ListViewState.EmptyState -> {
                Text(text = "No Data Found")
            }

            is ListViewState.Error -> {
                ( listState.value as ListViewState.Error).error.printStackTrace()
                Text(text = "Error, Please try again")
            }

            ListViewState.Loading -> {
                // show progress bar
            }

            is ListViewState.Success -> {
                val list = (listState.value as ListViewState.Success)?.list ?: emptyList()

                LazyColumn {

                    items(list.size) {
                        Text(
                            text = list[it].schoolName,
                            modifier = Modifier
                                .padding(16.dp)
                                .clickable {

                                })
                    }
                }


            }
        }


    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    MyApplicationTheme {
        Greeting("Android")
    }
}